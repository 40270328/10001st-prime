﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _10001st_Prime
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private static Boolean is_prime(int numToCheck)
        {
            int i = 5;

            if(numToCheck <= 1)
            {
                return false;
            }
            else if(numToCheck <= 3)
            {
                return true;
            }
            else if(numToCheck % 2 == 0 || numToCheck % 3 == 0)
            {
                return false;
            }

            while(i*i <= numToCheck)
            {
                if(numToCheck % i == 0 || numToCheck % (i+2) == 0)
                {
                    return false;
                }
                i = i + 6;
            }
            return true;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Boolean test;
            int count = 0, primeNum = 0;

            for (count = 0; primeNum < 10001; count++)
            {
                test = is_prime(count);

                if (test == true)
                {
                    primeNum++;
                }
            }

            lblAnswer.Content = "Answer: The Prime number " + (count-1) + " is the " + primeNum +"th number";
        }

    }
}
